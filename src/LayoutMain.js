import React, {Component} from 'react';
import LeftNav from './LeftNav.js';
import MainBody from './MainBody.js';

import './scss/LayoutMain.scss';

class LayoutMain extends Component {
	render() {
		return (
			<div class="layoutMain">
				<div className="navCol"><LeftNav/></div>
				<div className="bodyCol"><MainBody/></div>
			</div>
		)
	}
}

export default LayoutMain;
