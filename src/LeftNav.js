import React, {Component} from 'react';
import ServerButtons from './ServerButtons.js';
import ApplicationButtons from './ApplicationButtons.js';

import './scss/LeftNav.scss';

class LeftNav extends Component {
	render() {
		return (
			<div className="leftNav">
				<ServerButtons/>
				<ApplicationButtons/>
			</div>
		)
	}
}

export default LeftNav;