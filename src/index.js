import React from 'react';
import ReactDOM from 'react-dom';
import LayoutMain from './LayoutMain.js';

ReactDOM.render(
	<LayoutMain/>,
	document.getElementById('root')
);